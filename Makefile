CMD = ""

all: build

build:
	cargo build

list:
	cargo run list

new:
	cargo new $(CMD)

test:
	cargo test

clean-test:
	rm ./scripts/__unittest__debug__script__
