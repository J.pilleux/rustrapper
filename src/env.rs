use std::env::{self, VarError};
use std::io::Error;
use std::path::Path;

const SCRIPT_PATH: &str = "RUSTRAPPER_SCRIPT_DIR";

pub fn init_env(script_dir: &str) -> Result<(), Error> {
    let script_dir_path = Path::new(script_dir).to_path_buf();

    if !Path::is_dir(&script_dir_path) {
        let err_msg = format!(
            "The path {:?} does not exist or is not a directory",
            script_dir_path.as_path()
        );
        return Err(Error::new(std::io::ErrorKind::Other, err_msg));
    }

    env::set_var(SCRIPT_PATH, &script_dir_path);

    Ok(())
}

pub fn get_script_dir() -> Result<String, VarError> {
    Ok(env::var(SCRIPT_PATH)?)
}
