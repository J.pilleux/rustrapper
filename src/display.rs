use colored::Colorize;

const HEADER_SIZE: usize = 60;
const PADDING: usize = 1;
const FILLER: &str = "-";
const SPACING: &str = "    ";
const LIST_ENTRY: &str = "> ";

pub fn format_error(msg: &str) -> String {
    format!("{}: {}", "Error".red(), msg)
}

pub fn format_success(msg: &str) -> String {
    format!("{}: {}", "Success".green(), msg)
}

fn get_filler(txt_size: usize) -> String {
    let filler_size = (HEADER_SIZE - (txt_size + (2 * PADDING))) / 2;
    String::from(FILLER.repeat(filler_size))
}

pub fn format_header(txt: &str) -> String {
    if txt.len() >= HEADER_SIZE {
        return format!("{}", txt.yellow());
    }

    let filler = get_filler(txt.len());
    format!("{0} {1} {0}", &filler.cyan(), txt.yellow())
}

fn format_list_entry(entry: String) -> String {
    format!("{}{}{}", SPACING, LIST_ENTRY.yellow(), entry)
}

fn format_list(list: Vec<String>) -> String {
    let mapped: Vec<String> = list.iter().map(|s| format_list_entry(s.to_string())).collect();

    mapped.join("\n")
}

pub fn format_list_header(header: &str, list: Vec<String>) -> String {
    format!("{}\n\n{}", &format_header(header), &format_list(list))
}
