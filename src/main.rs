use colored::Colorize;
use std::process;

use crate::args::parse_args;
use rustrapper::{
    commands::{
        describe::describe_script, edit::edit_script, install::run_script_install,
        list::list_scripts, new::new_script_file, uninstall::run_script_uninstall,
    },
    display::{format_error, format_header, format_list_header, format_success},
    env::{get_script_dir, init_env},
};

mod args;

extern crate rustrapper;

fn print_script_list() {
    let script_list = match list_scripts() {
        Ok(list) => list,
        Err(err) => {
            eprintln!("{}", format_error(&err.to_string()));
            process::exit(1);
        }
    };

    if script_list.is_empty() {
        println!(
            "No scrpits were found in: {}",
            get_script_dir().unwrap().blue()
        );
    } else {
        println!("{}", format_list_header("Script list", script_list));
        println!("\nScripts found in: {}", get_script_dir().unwrap().blue());
    }
    process::exit(0);
}

fn create_new_script(name: &str) {
    match new_script_file(name.trim()) {
        Ok(_) => println!("{}", format_success(&format!("Script {} created", &name))),
        Err(err) => eprintln!("{}", format_error(&err.to_string())),
    }
}

fn install(script_name: &str) {
    println!("{}", format_header(&format!("Installing {}", script_name)));
    match run_script_install(script_name.trim()) {
        Ok(_) => {
            let msg = format!("Script {} installed", script_name.blue());
            println!("{}", &format_success(&msg));
        }
        Err(err) => eprintln!("{}", format_error(&err.to_string())),
    }
}

fn describe(script_name: &str) {
    match describe_script(script_name.trim()) {
        Ok(..) => process::exit(0),
        Err(err) => eprintln!("{}", format_error(&err.to_string())),
    }
}

fn uninstall(script_name: &str) {
    println!(
        "{}",
        format_header(&format!("Uninstalling {}", script_name))
    );
    match run_script_uninstall(script_name) {
        Ok(_) => {
            let msg = format!("Script {} uninstalled", script_name.blue());
            println!("{}", &format_success(&msg));
        }
        Err(err) => eprintln!("{}", format_error(&err.to_string())),
    }
}

fn multiple(scripts: &str) {
    let script_list: Vec<&str> = scripts.split(" ").collect();
    for script in script_list {
        install(script);
    }
}

fn edit(script_name: &str) {
    match edit_script(script_name.trim()) {
        Ok(..) => println!(
            "{}",
            format_success(&format!("Script {} saved", script_name))
        ),
        Err(err) => eprintln!("{}", format_error(&err.to_string())),
    }
}

fn main() {
    let args = parse_args();

    if let Err(err) = init_env(&args.script_dir) {
        eprintln!("{}", format_error(&err.to_string()));
        process::exit(1);
    }

    match &args.command {
        args::Commands::New { script_name } => create_new_script(&script_name),
        args::Commands::Install { script_name } => install(&script_name),
        args::Commands::Uninstall { script_name } => uninstall(&script_name),
        args::Commands::Describe { script_name } => describe(&script_name),
        args::Commands::List => print_script_list(),
        args::Commands::Multiple { script_list } => multiple(&script_list),
        args::Commands::Edit { script_name } => edit(&script_name),
    }
}
