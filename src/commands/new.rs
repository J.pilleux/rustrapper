use std::fs;

use anyhow::Result;

use super::{script_template::FILE_TEMPLATE, common::assert_script_not_exists};


pub fn new_script_file(script_name: &str) -> Result<()> {
    let script_path = assert_script_not_exists(script_name)?;
    fs::write(script_path, FILE_TEMPLATE)?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use crate::env::get_script_dir;

    use super::*;
    const FILE_NAME: &str = "__unittest__debug__script__";

    #[test]
    fn new_script_test() {
        let expected_path = format!("{}/{}", get_script_dir().unwrap(), FILE_NAME);
        let res = new_script_file(FILE_NAME);

        assert!(res.is_ok());
        assert!(Path::new(&expected_path).exists());
        let actual_content = fs::read_to_string(&expected_path).unwrap();
        assert_eq!(FILE_TEMPLATE, actual_content);

        // Creating another
        let res = new_script_file(FILE_NAME);
        assert!(!res.is_ok());

        // Cleaning mess
        fs::remove_file(&expected_path).unwrap();
    }
}
