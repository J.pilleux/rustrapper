use anyhow::Result;

use super::common::run_script;

pub fn run_script_install(name: &str) -> Result<()> {
    run_script(name, "-i")
}
