use std::{fs::read_dir, path::Path};

use crate::env::get_script_dir;
use anyhow::Result;

pub fn list_scripts() -> Result<Vec<String>> {
    let script_path = get_script_dir()?;

    let script = read_dir(&script_path)?;
    let mut script_names: Vec<String> = Vec::new();

    for path in script {
        let t = match path {
            Err(_) => continue,
            Ok(p) => p
        };

        if !Path::is_file(&t.path()) {
            continue;
        }

        match t.path().file_name() {
            None => continue,
            Some(name) => {
                match name.to_str() {
                    None => continue,
                    Some(f) => script_names.push(String::from(f))
                }
            }
        }
    }

    Ok(script_names)
}
