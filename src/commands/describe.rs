use anyhow::Result;

use super::common::run_script;

pub fn describe_script(script_name: &str) -> Result<()> {
    run_script(script_name, "-d")
}
