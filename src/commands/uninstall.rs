use super::common::run_script;
use anyhow::Result;

pub fn run_script_uninstall(name: &str) -> Result<()> {
    run_script(name, "-u")
}
