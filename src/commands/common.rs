use std::path::{Path, PathBuf};

use anyhow::{anyhow, Result};
use subprocess::Exec;

use crate::env::get_script_dir;

pub fn get_script_full_path(script_name: &str) -> Result<String> {
    let script_dir = &get_script_dir()?;
    Ok(format!("{}/{}", script_dir, script_name))
}

pub fn assert_script_not_exists(script_name: &str) -> Result<PathBuf> {
    let path = get_script_full_path(script_name)?;
    let script_path = Path::new(&path);

    if Path::exists(&script_path) {
        return Err(anyhow!("The script {:?} already exist", script_path));
    }

    Ok(script_path.to_path_buf())
}

pub fn run_script(name: &str, opt: &str) -> Result<()> {
    let script_path = get_script_full_path(name)?;

    if !Path::new(&script_path).exists() {
        return Err(anyhow!("The script {} does not exists", &script_path));
    }

    Exec::cmd("bash").arg(&script_path).arg(opt).join()?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    const FILE_NAME: &str = "__unittest__debug__script__";

    #[test]
    fn assert_script_not_exists_test() {
        let expected_path = format!("{}/{}", get_script_dir().unwrap(), FILE_NAME);
        let res = assert_script_not_exists(&expected_path);
        assert!(res.is_ok());
    }

    #[test]
    fn get_script_full_path_test() {
        let actual = get_script_full_path(FILE_NAME).unwrap();
        let expected = "./scripts/__unittest__debug__script__";

        assert_eq!(expected, actual);
    }
}
