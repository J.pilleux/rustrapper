use std::env;

use anyhow::Result;
use subprocess::Exec;

use super::common::get_script_full_path;

pub fn edit_script(script_name: &str) -> Result<()> {
    let script_path = get_script_full_path(script_name)?;
    let editor_cmd = env::var("EDITOR")?;
    Exec::cmd(editor_cmd).arg(&script_path).join()?;

    Ok(())
}
