use std::{env, process};

use clap::{Parser, Subcommand};

const DEFAULT_SCRIPT_DIR: &str = "/.dotfiles/_other/rustrapper";

fn get_default_script_dir() -> String {
    let usr_home = match env::var("HOME") {
        Ok(value) => value,
        Err(_) => {
            eprintln!("Cannot get the environment variable $HOME");
            process::exit(1);
        }
    };

    format!("{}{}", usr_home, DEFAULT_SCRIPT_DIR)
}

#[derive(Parser)]
#[clap(author, version, about)]
pub struct Cli {
    #[clap(short, long, default_value_t = get_default_script_dir(), value_name = "SCRIPT_DIR")]
    pub script_dir: String,
    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
    /// Create a new empty command
    New {
        #[clap(value_parser, value_name = "SCRIPT_NAME")]
        script_name: String,
    },
    /// Install a command
    Install {
        #[clap(value_parser, value_name = "SCRIPT_NAME")]
        script_name: String,
    },
    /// Uninstall (if possible) a command
    Uninstall {
        #[clap(value_parser, value_name = "SCRIPT_NAME")]
        script_name: String,
    },
    /// Describe a command
    Describe {
        #[clap(value_parser, value_name = "SCRIPT_NAME")]
        script_name: String,
    },
    /// List available commands
    List,
    /// Run multiple commands
    Multiple {
        #[clap(value_parser, value_name = "SCRIPT_NAMES")]
        script_list: String,
    },
    /// Edit a command
    Edit {
        #[clap(value_parser, value_name = "SCRIPT_NAME")]
        script_name: String,
    },
}

pub fn parse_args() -> Cli {
    Cli::parse()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn verify_cli() {
        use clap::CommandFactory;
        Cli::command().debug_assert()
    }
}
